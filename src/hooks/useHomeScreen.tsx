import * as React from 'react';

export const useHomeScreen = () => {

    const INDICADORES  = [
        {nombre:"DOLAR",id:"1",icon:require("../assets/icons/grafico-de-barras.png")},
        {nombre:"EURO",id:"2",icon:require("../assets/icons/grafico-de-barras.png")},
        {nombre:"IPC",id:"3", icon:require("../assets/icons/grafico-de-barras.png")},
        {nombre:"UF",id:"4", icon:require("../assets/icons/grafico-de-barras.png")},
        {nombre:"UTM",id:"5",icon:require("../assets/icons/grafico-de-barras.png")},
    ]
   

    return {
        INDICADORES

    }
}

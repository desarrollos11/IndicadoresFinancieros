import axios from "axios";

export const CMFChileAPI = axios.create({
    //baseURL: 'https://api.sbif.cl/api-sbifv3/recursos_api',
    baseURL: 'https://api.cmfchile.cl/api-sbifv3/recursos_api'
});
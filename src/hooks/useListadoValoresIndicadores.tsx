import * as React from 'react';
import { CMFChileAPI } from '../API/CmfChileAPI';
import { Helper } from '../helpers/Helper';

export const useListadoValoresIndicadores = () => {
    const [dataIndicadoresState, setDataIndicadorState] = React.useState()

    const {anteponerCero} = Helper()

    const getIndicadorDetalleInfo = async (indicador:string) => {
        const date = new Date();
        const diaActual = anteponerCero(date.getDate())
        const mesActual = anteponerCero(date.getUTCMonth()+1)
        const anioActual = (date.getFullYear())

        date.setDate(date.getDate()-30)
        const diaIniciall = anteponerCero(date.getDate())
        const mesInicial =  anteponerCero(date.getUTCMonth()+1)
        const anioInicial =(date.getFullYear())

        let resp;
        switch(indicador){
            case "DOLAR":
                 resp = await CMFChileAPI.get('/dolar/periodo/'+anioInicial+'/'+mesInicial+'/dias_i/'+diaIniciall+'/'+anioActual+'/'+mesActual+'/dias_f/'+diaActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                setDataIndicadorState(resp.data.Dolares)
            break;
            case "EURO":
                resp = await CMFChileAPI.get('/euro/periodo/'+anioInicial+'/'+mesInicial+'/dias_i/'+diaIniciall+'/'+anioActual+'/'+mesActual+'/dias_f/'+diaActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                setDataIndicadorState(resp.data.Euros)
            break;
            case "IPC":
                resp = await CMFChileAPI.get('/ipc/'+anioActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                setDataIndicadorState(resp.data.IPCs)
            break;
            case "UTM":
                resp = await CMFChileAPI.get('/utm/'+anioActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                setDataIndicadorState(resp.data.UTMs)
            break;
            case "UF":
                console.log('/uf/periodo/'+anioActual+'/'+mesActual+'/'+anioInicial+'/'+mesInicial+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                resp = await CMFChileAPI.get('/uf/periodo/'+anioInicial+'/'+mesInicial+'/'+anioActual+'/'+mesActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                console.log(resp.data)
                setDataIndicadorState(resp.data.UFs)      
            break;
        }
    }
   

    return {
        dataIndicadoresState,
        getIndicadorDetalleInfo

    }
}

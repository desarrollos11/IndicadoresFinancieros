import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import  React from "react"
import { COLORS, SIZES, FONTS } from "../assets/theme"
import icons from "../constants/icons";
import { StackScreenProps } from '@react-navigation/stack'
import { RootStackParams } from "../../App";
import { useHomeScreen } from "../hooks/useHomeScreen";
import { ItemSeparator } from "../components/ItemSeparator";
import { Container } from "../components/Container";

interface Props extends StackScreenProps<RootStackParams,'Home'>{}

const HomeScreen = ({navigation}:Props) => {

  const {INDICADORES} = useHomeScreen()
 // const GLOBAL = require('../Globals');
    
    return (
      <Container>   
        <View>     
          <Text style={{alignSelf:'center', fontSize:12,letterSpacing:1,color:COLORS.primary,marginBottom:20}}>Por favor, seleccione un indicador</Text>
          <FlatList
            data={INDICADORES}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => 
              <TouchableOpacity
                  style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingVertical: SIZES.base,
                      marginBottom:4
                  }}
                  onPress={() => {
                    navigation.navigate("ListadoValoresIndicadores",{indicador:item.nombre})
                    console.log(item)}
                  }
                >
                  <TouchableOpacity onPress={() => {navigation.navigate("DetalleIndicador",{indicador:item.nombre})}}>
                    <Image
                      source={item.icon}
                      style={{
                          width: 30,
                          height: 30,
                      }}
                    />
                    <Text style={{
                      fontSize:8,
                      color:COLORS.primary
                    }
                    }>Ver Detalles</Text>
                  </TouchableOpacity>
              

                    <View style={{ flex: 1, marginLeft: SIZES.radius }}>
                        <Text style={{ marginStart:30,letterSpacing:0.5,fontWeight:"600"}}>{item.nombre}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', height: '100%', alignItems: 'center' }}>
                      <Text style={{
                          fontSize:8,
                          color:COLORS.primary
                        }
                        }>Valores</Text>
                        <Image
                            source={icons.right_arrow}
                            style={{
                                width: 20,
                                height: 20,
                                tintColor: COLORS.primary
                            }}
                        />
                        
                    </View>
                </TouchableOpacity>
            }
            keyExtractor={item => item.id}
            ItemSeparatorComponent={ItemSeparator}
          />
        </View>
      </Container>



  
     
    );
  };
  
  
  export default HomeScreen;
import React from 'react'
import { View } from 'react-native'
import { COLORS, SIZES } from '../assets/theme'

interface ContainerOpacityType{
    children : JSX.Element
}

export const Container = ({children}:ContainerOpacityType) => {
  return (
    <View
      style={{
          marginTop: SIZES.padding,
          marginHorizontal: SIZES.padding,
          padding: 20,
          borderRadius: SIZES.radius,
          backgroundColor: COLORS.white,
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 4,
            },
            shadowOpacity: 0.30,
            shadowRadius: 4.65,
    
            elevation: 8,
      }}>
    {children}
    </View>     
  )
}

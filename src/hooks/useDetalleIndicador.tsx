import * as React from 'react';
import { Alert } from 'react-native';
import { CMFChileAPI } from '../API/CmfChileAPI';
import { Helper } from '../helpers/Helper';
interface valorActual {
    Valor:"",
    Fecha:""
}

interface itemIndicador {
         valores: number[];
         fechas: string[];
         valorActual:valorActual,
         indicador:string,
         cargado:boolean
 }
export const useDetalleIndicador = () => {

    const [indicadorState, setIndicadorState] = React.useState<itemIndicador>({
        indicador:"",
        valorActual:{
            Valor:"",
            Fecha:""
        },
        valores:[],
        fechas:[],
        cargado:false
    })
    const {anteponerCero} = Helper()
    
    const getValorIndicador = async (indicador:string) => {
        const date = new Date();
        const diaActual = anteponerCero(date.getDate())
        const mesActual = anteponerCero(date.getUTCMonth()+1)
        const anioActual = (date.getFullYear())

        date.setDate(date.getDate()-10)
        const diaIniciall = anteponerCero(date.getDate())
        const mesInicial =  anteponerCero(date.getUTCMonth()+1)
        const anioInicial =(date.getFullYear())
        
        let arrayValores : number[] =  []
        let arrayFechas :string[] =  []

        let resp
        let item
        switch(indicador)
        {
            case "DOLAR":
                resp = await CMFChileAPI.get('/dolar/periodo/'+anioInicial+'/'+mesInicial+'/dias_i/'+diaIniciall+'/'+anioActual+'/'+mesActual+'/dias_f/'+diaActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                item = resp.data.Dolares
            break;

            case "EURO":
                resp = await CMFChileAPI.get('/euro/periodo/'+anioInicial+'/'+mesInicial+'/dias_i/'+diaIniciall+'/'+anioActual+'/'+mesActual+'/dias_f/'+diaActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                item = resp.data.Euros
            break;

            case "UTM":
                resp = await CMFChileAPI.get('/utm/'+anioActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                item = resp.data.UTMs
            break;
            case "UF":
                let position 
                let array = []
                resp = await CMFChileAPI.get('/uf/periodo/'+anioInicial+'/'+mesInicial+'/'+anioActual+'/'+mesActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                item = resp.data.UFs
                for(let i=1;i<10;i++)
                {
                    position = item.length-i
                    array.push(item[position])
                }
                item = array

            break;
            case "IPC":
                resp = await CMFChileAPI.get('/ipc/'+anioActual+'?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                item = resp.data.IPCs
            break;

        }
              
        for(let i = 0;i<item.length;i++)
        {
            const date = new Date(item[i].Fecha);
            const mes = anteponerCero(date.getMonth()+1)
            const dia = anteponerCero(date.getDate())
            let valor
            arrayFechas.push(mes+"-"+dia)
            
            if(indicador!="UTM" && indicador!="UF")
            {
                 valor = item[i].Valor.toString().replace(",",".")
            }
            else{
                valor = item[i].Valor.toString().replace(".","")
            }
            valor = (parseFloat(valor))
            arrayValores.push(valor)
        }
        getValorActualIndicador(indicador).then((valorActual) => {
            setIndicadorState({
                valorActual:{
                    Valor:valorActual[0].Valor,
                    Fecha:valorActual[0].Fecha,
                },
                valores:arrayValores,
                fechas:arrayFechas,
                indicador:indicador,
                cargado:true
            })
        })
      
    }

    const getValorActualIndicador = async(indicador:string) => {
        let ok = false
        let intento = 0
        
        while(!ok)
        {
            try{
                const date = new Date()
                date.setDate(date.getDate()-intento)
                const dia = anteponerCero(date.getDate())
                const mes =  anteponerCero(date.getUTCMonth()+1)
                const anio =(date.getFullYear())
                const resp = await CMFChileAPI.get("/"+indicador.toLocaleLowerCase()+'/'+anio+'/'+mes+'/dias/'+dia+'/?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                let item
                switch(indicador)
                {
                    case "DOLAR":
                        item = resp.data.Dolares
                    break;

                    case "EURO":
                        item = resp.data.Euros
                    break;
                    case "UTM":
                        const responseUTM = await CMFChileAPI.get('/utm?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                        item = responseUTM.data.UTMs
                    break;
                    case "UF":
                        item = resp.data.UFs
                    break;

                    case "IPC":
                        const responseIPC = await CMFChileAPI.get('/ipc?apikey=4357877a111ea5a89ac6988ef029abbab0f051f3&formato=json')
                        item = responseIPC.data.IPCs                    
                        break;
                }
                return item
            }
            catch(e:any)
            {
                intento ++
            }
        }
        return null
    }
   

    return {
        indicadorState,
        getValorIndicador
        
    }
}

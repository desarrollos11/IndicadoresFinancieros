const back_arrow = require("../assets/icons/back_arrow.png");
const line_graph = require("../assets/icons/line_graph.png");
const pie_chart = require("../assets/icons/pie_chart.png");
const right_arrow = require("../assets/icons/right_arrow.png");
const transaction = require("../assets/icons/transaction.png");

export default {
    back_arrow,
    line_graph,
    pie_chart,
    right_arrow,
    transaction
}
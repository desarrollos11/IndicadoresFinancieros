import { StackScreenProps } from "@react-navigation/stack";
import React from "react";
import { View,Text, Dimensions } from "react-native";
import {
    LineChart,
    //BarChart,
    //PieChart,
    //ProgressChart,
    //ContributionGraph,
    //StackedBarChart
  } from "react-native-chart-kit";
import { RootStackParams } from "../../App";
import { COLORS } from "../assets/theme";
import { Container } from "../components/Container";
import { Loading } from "../components/Loading";
import { useDetalleIndicador } from "../hooks/useDetalleIndicador";
interface Props extends StackScreenProps<RootStackParams,'DetalleIndicador'>{}

export const DetalleIndicador = ({route}:Props) => {
    const { indicadorState,getValorIndicador } = useDetalleIndicador()
    React.useEffect(() => {
        getValorIndicador(route.params.indicador)
    }, [])


    if(indicadorState.cargado)
    {
    
        return (
            <View>
                <Container>
                    <View>
                        <Text style={{fontSize:24,fontWeight:"300",marginTop:5,textAlign:"center",color:COLORS.primary}}>{indicadorState.indicador}</Text>
                        <View style={{height:0.4,width:300,alignSelf:"center",backgroundColor:COLORS.primary}}></View>
                        <View style={{flexDirection:"row",alignSelf:"center"}}>
                            <Text style={{fontSize:24}}>$</Text>
                            <Text style={{fontSize:24,textAlign:"center"}}>{indicadorState.valorActual.Valor}</Text>
                        </View>
                        <View style={{flexDirection:"row",alignSelf:"center"}}>
                            <Text style={{fontSize:10,fontWeight:"200"}}>Valor Actual</Text>
                        </View>
                        <View style={{flexDirection:"row",alignSelf:"center"}}>
                            <Text style={{fontSize:10}}>Fecha:</Text>
                            <Text style={{fontSize:10,textAlign:"center"}}>{indicadorState.valorActual.Fecha}</Text>
                        </View>
                    </View>        
                </Container>        
                <Container>
                    <View>
                   
                        <Text style={{ marginStart:4,fontSize:15,fontWeight:"600",color:COLORS.primary }}>Ultimos valores 2022</Text>
                        <LineChart
                            data={{
                                labels:indicadorState.fechas,
                                datasets: [
                                {
                                    data:indicadorState.valores
                                }
                                ]
                            }}
                            width={Dimensions.get("window").width-100} // from react-native
                            height={220}
                            yAxisLabel="$"
                            yAxisSuffix=""
                            yAxisInterval={1} // optional, defaults to 1
                            chartConfig={{
                                backgroundColor: COLORS.primary,
                                backgroundGradientFrom: COLORS.primary,
                                backgroundGradientTo: COLORS.primary,
                                color: (opacity = 1) =>COLORS.white,
                                labelColor: (opacity = 1) => COLORS.white,
                                style: {
                                borderRadius: 16
                                },
                                propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#ffa726"
                                }
                            }}
                            bezier
                            style={{
                                marginVertical: 8,
                                borderRadius: 16
                            }}
                        />
                    </View>
                </Container>
            </View>
        );
    }
    else{
        return <Loading></Loading>
        
    }
}

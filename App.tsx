/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { styles } from './src/assets/styles';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './src/screens/HomeScreen';
import { ListadoValoresIndicadores } from './src/screens/ListadoValoresIndicadores';
import { DetalleIndicador } from './src/screens/DetalleIndicador';


const Section: React.FC<{
  title: string;
}> = ({children, title}:any) => {
  const isDarkMode = useColorScheme() === 'dark';
  
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};
export type RootStackParams = {
  Home: undefined,
  ListadoValoresIndicadores:{indicador:string}
  DetalleIndicador:{indicador:string}

}
const Stack = createNativeStackNavigator<RootStackParams>();


const App = () => {
  return (
    <NavigationContainer> 
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home"   options={{ title: 'Indicadores' }} component={HomeScreen} />
        <Stack.Screen name="ListadoValoresIndicadores"   options={{ title: 'Detalle Indicador' }} component={ListadoValoresIndicadores} />
        <Stack.Screen name="DetalleIndicador"   options={{ title: 'Detalle Indicador' }} component={DetalleIndicador} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};


export default App;

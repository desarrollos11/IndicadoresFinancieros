import React from 'react'

export const Helper = () => {
    const anteponerCero = (numero:number) => {

        let respond = "";
        if((numero) <10)
        {
            respond="0"+numero
        }
        else
        {
            respond = numero.toString()
        }
        return respond
    }
  return {
    anteponerCero

  }
  
}

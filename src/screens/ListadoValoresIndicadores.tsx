import { StackScreenProps } from '@react-navigation/stack';
import React from 'react'
import { Image, Text, View, FlatList, TouchableOpacity, Dimensions  } from 'react-native'
import { RootStackParams } from '../../App';
import { COLORS, FONTS, SIZES } from '../assets/theme';
import { Container } from '../components/Container';
import { ItemSeparator } from '../components/ItemSeparator';
import icons from '../constants/icons';
import { useListadoValoresIndicadores } from '../hooks/useListadoValoresIndicadores';
interface Props extends StackScreenProps<RootStackParams,'ListadoValoresIndicadores'>{}

export const ListadoValoresIndicadores = ({route}:Props) => {
    const { dataIndicadoresState,getIndicadorDetalleInfo } = useListadoValoresIndicadores()

    React.useEffect(() => {
        getIndicadorDetalleInfo(route.params.indicador)
    }, [])
    const renderItemDetalleIndicador = (item:any) => {
     
        return (
            <TouchableOpacity
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    alignSelf:"center",
                    paddingVertical: SIZES.base,
                    marginBottom:4
                }}
                onPress={() => {
                  console.log(item)}}
                >
                <Image
                    source={require("../assets/icons/variation.png")}
                    style={{
                        width: 15
                        ,
                        height: 15,
                        tintColor: COLORS.primary
                    }}
                />
                <View style={{ flex: 1, marginLeft: SIZES.radius }}>
                    <Text style={{ marginStart:4,fontWeight:"bold",...FONTS.h4 }}>{item.Fecha}</Text>
                </View>
                <View style={{ flex: 1, marginLeft: SIZES.radius,flexDirection:"row" }}>
                    <Text style={{ marginStart:0,fontWeight:"400",color:COLORS.primary, fontSize:14, marginEnd:5}}>$</Text>
                    <Text style={{ marginStart:0,fontWeight:"bold",...FONTS.h4 }}>{item.Valor}</Text>
                </View>
            </TouchableOpacity>
        )
    }


    return (
        <Container> 
            <View>
                <Text style={{alignSelf:'center', fontSize:12,letterSpacing:1,color:COLORS.primary,marginBottom:20}}>Ultimos Valores</Text>
                <View style={{flexDirection:"row"}}>
                    <View style={{
                        paddingHorizontal:50
                    }}>
                        <Text style={{fontSize:12,letterSpacing:1,color:COLORS.primary}}>Fecha</Text>
                    </View>
                    <View style={{
                        paddingHorizontal:60
                    }}>
                        <Text style={{fontSize:12,letterSpacing:1,color:COLORS.primary,marginStart:100}}>Valor</Text>
                    </View>
                </View>

                <View style={{padding:30}}>
                <FlatList
                    data={dataIndicadoresState}
                style={{height:Dimensions.get("screen").height-450}}
                    showsVerticalScrollIndicator={false}
                    renderItem={({item}) => renderItemDetalleIndicador(item)
                        
                    }
                    //keyExtractor={item => item.id}
                    ItemSeparatorComponent={ItemSeparator}
                    />
                </View>
            </View>
        </Container>

           
  
  
  
    
       
      );
}
